package steuerung;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import ausnahmen.BeschreibungException;
import ausnahmen.KeinWortException;
import ausnahmen.SyntaxException;


/**
 * Die verwendete Datenstruktur fuer die Wortliste ist eine Kreuzung aus einer TreeMap und
 * ein HashSet, da HashSets keine Duplikate zulassen und eine TreeMap Strings sortiert.
 * Diese Klasse ist ausserdem fuer die direkte manipulation der Wortliste zustaendig.
 */
public class Wortliste {
	
	/** Hier werden jedem Loesungswort als String (Key), alle moeglichen
	 * Beschreibungen als HashSet (Value) zugeordnet */
    private TreeMap<String,HashSet<String>> wortliste;

    /**
     * Dieser Konstruktor erzeugt eine leere Wortliste
     */
    public Wortliste(){
    	wortliste = new TreeMap<String,HashSet<String>>();
    }
    
    /** 
     * Benutzt die alte Wortliste weiter, um aus der uebergebenen Datei die eingeladenen
     * Elemente der aktuellen Wortliste hinzuzufuegen, wobei Duplikate nicht zugelassen sind.
     * Es koennen mehrere unterschiedliche Wortlisten eingelesen werden.
     * 
     * @param dateiname Dateiname der Datei die eingelesen wird
     * @throws FileNotFoundException Keine Datei gefunden
     * @throws IOException Einlesefehler
     */
    void ladeListe(File dateiname) throws FileNotFoundException,IOException {
        /* Temporaere Map, die alle Woerter von der Datei einliesst und spaeter
         * der eigentlichen TreeMap hinzufuegt, indem eine neue TreeMap erstellt
         * wird */
        TreeMap<String,HashSet<String>> temp = new TreeMap<String,HashSet<String>>();
        FileReader fr = new FileReader(dateiname);      
        BufferedReader b = new BufferedReader(fr);

        /* Pruefe, ob bereits eine Wortliste existiert zu der eine neue Wortliste
         * hinzugefuegt wird. So uebernehme die alte Wortliste mit */
        if (wortliste.isEmpty() == false){
        	temp.putAll(wortliste);
        }

        boolean einlesen = true;
        while(einlesen == true) {
            String zeile = b.readLine();
            
            /* Datei ist zuende */
            if (zeile == null){
            	einlesen = false;
            	break;
            }
            
            /* Nehme Zeile entgegen und speichere das Loesungswort und die
             * zugehoerige Beschreibung in ein Hilfsarray ab
             * wort[0] = Loesungswort
             * wort[1] = Wortbeschreibung
             */
            String[] wort = zeile.split("\\s+",2);

            /* Keine Loesungswortbeschreibung */
            if (wort.length == 1) {
            	//System.out.println("Keine Beschreibung vorhanden");
            } else {
                try {
                	/* Prueft, ob Loesungswort und Beschreibung in Ordnung sind */
                    String w = pruefeLoesungswort(wort[0]);
                    pruefeBeschreibung(wort[1]);
                    
                    /* Gibt es dieses Loesungswort bereits, dann fuege 
                     * die Beschreibung hinzu */
                    if (temp.containsKey(w)){
                        temp.get(w).add(wort[1]);
                    } else {
                    	/* Fuege neues Loesungswort samt Beschreibung
                    	 * der Map hinzu */
                        HashSet<String> beschreibungen = new HashSet<String>();
                        beschreibungen.add(wort[1]);
                        temp.put(w, beschreibungen);
                    }
                }
                catch (KeinWortException e1) {
                    //System.out.println("Eine Leerzeile");
                }
                catch (SyntaxException e2) {
                	//System.out.println("Ungueltiges Symbol");
                }
                catch (BeschreibungException e3) {
                	//System.out.println("Keine Beschreibung / Beschreibung zu lang");
                }
            }
        }
        b.close();       
        /* Zu der bestehenden Woerterliste werden alle Woerter die in der
         * temporaeren Liste (weitere Wortliste) vorkommen hinzugefuegt */
        wortliste.putAll(temp);
    }
    
    /**
     * loescht die aktuelle Wortliste komplett, d.h. die TreeMap wird geleert
     */
    void wortlisteLoeschen(){
    	wortliste.clear();
    }
    
    /**
     * Hier wird ein neues Loesungswort samt Beschreibung eingefuegt. Sollte ein
     * Loesungswort bereits Beschreibungen enthalten, so werden zu dem passenden
     * Loesungswort nur die Beschreibungen hinzugefuegt, die noch nicht existieren.
     * 
     * @param loesungswort Das neue Loesungswort
     * @param beschreibungen Die neue Beschreibung
     * @throws KeinWortException Kein Loesungsort (Leerzeile)
     * @throws SyntaxException unzulaessige Zeichen im Loesungswort
     * @throws BeschreibungException Beschreibung hat keine oder zuviele Zeichen
     */
    public void wortHinzufuegen(String loesungswort, HashSet<String> beschreibungen) throws KeinWortException, SyntaxException, BeschreibungException {
        if(wortliste.containsKey(loesungswort) == true) {
        	
        	/* Pruefe, ob eingegebene Beschreibung gueltig ist */
            pruefeBeschreibungen(beschreibungen);
            
            /* Ermittel vorhandene Beschreibungen zu ein Loesungswort und fuege
             * die neuen Beschreibungen dem HashSet hinzu und anschliessend
             * der Wortliste */
            HashSet<String> h = wortliste.get(loesungswort);
            h.addAll(beschreibungen);
            wortliste.put(loesungswort, h);
        } else {
            pruefeLoesungswort(loesungswort);
            pruefeBeschreibungen(beschreibungen);
            
            /* Es ist ein neues Loesungswort samt Beschreibung(en)
             * hinzugekommen */
            wortliste.put(loesungswort, beschreibungen);
        }        
    }

    /**
     * hier wird das uebergebene Loesungswort aus der Wortliste geloescht
     * 
     * @param wort Wort was geloescht wird
     */
    public void wortLoeschen(String wort) {
    	if (wortliste.containsKey(wort) == true){
    		wortliste.remove(wort);
    	}
    }
    
    /**
     * hier wird die aktuelle Wortliste in eine Datei abgespeichert.
     * 
     * @param datei zu speichernde Datei
     * @throws IOException Speicherfehler
     */
    void speichereWortliste(File datei) throws IOException {
    	FileWriter fw = new FileWriter(datei);
        BufferedWriter b = new BufferedWriter(fw);
        for (Iterator<String> wort = wortliste.keySet().iterator(); wort.hasNext();) {
            String w = wort.next();
            for (Iterator<String> beschreibungen = wortliste.get(w).iterator(); beschreibungen.hasNext();) {
                /* speicher zuerst das Loesungsowrt */
            	b.write(w);
                b.write(" ");
                
                /* beschreibungen zu diesen Loesungswort */
                b.write(beschreibungen.next());
                b.newLine();
            }
        }
        b.close();
    }
    
    /**
     * uebergibt die Anzahl der Loesungswoerter die im Raetsel vorkommen sollen und
     * waehlt zufaellig diese Anzahl an Loesungswoertern aus der Wortliste aus.
     * 
     * @param anzahl Zahl der Pflichtwoerter die im Raetsel vorkommen sollen
     * @return Wortlistearray, wobei der erste Eintrag alle nicht ausgewaehlten
     * Loesungswoerter aus der aktuellen Wortliste enthaelt, indem alle ausgewaehlten Woerter 
     * in den zweiten Eintrag gespeichert werden und zugleich aus den ersten Eintrag 
     * geloescht werden, da keine Duplikate zulaessig sind.
     *         
     *  <li><code>Wortliste liste = {(alle Woerter aus der Wortliste - ausgewaehlte Woerter), ausgewaehlte Woerter fuer das Raetsel} </code></li>
     */
    public Tupel<Wortliste> waehleWoerter(int anzahl) {
        Wortliste woerterliste = getKopie();
        Wortliste auswahlliste = new Wortliste();
        int laenge = woerterliste.getAnzahlLoesungswoerter();
        if (laenge < anzahl) {
        	
        	/* Es sind nicht genug Woerter vorhanden. Es werden alle
        	 * Woerter ausgewaehlt */
        	Tupel<Wortliste> ergebnis = new Tupel<Wortliste>(auswahlliste, woerterliste);
        //    Wortliste[] ergebnis = new Wortliste[]{auswahlliste, woerterliste};
            return ergebnis;
        }
        
        for (int i=0; i<anzahl; i++) {
        	
        	/* Suche nach allen Woertern aus der Wortliste */
            ArrayList<String> woerter = woerterliste.getWortsuche("");
            String loesungswort = woerter.get((int) Math.floor(Math.random()*laenge));
            
            /* Suche fuer das Loesungswort eine passende Beschreibung */
            HashSet<String> beschreibung = waehleBeschreibung(loesungswort);
            try {
                auswahlliste.wortHinzufuegen(loesungswort, beschreibung);
            } catch (Exception e) {}
            
            /* Damit keine Duplikate vorkommen koennen, wird das ausgewaehlte
             * Loesungswort aus der kopierten Liste geloescht */
            woerterliste.wortLoeschen(loesungswort);
            laenge--;
        }
        
    	Tupel<Wortliste> ergebnis = new Tupel<Wortliste>(woerterliste,auswahlliste);
        return ergebnis;
    }
    
    /**
     * Waehlt zufaellig eine Beschreibung zu einem gegebenen Loesungswort aus.
     * 
     * @param loesungswort Das Loesungswort, fuer das eine Beschreibung ausgewaehlt werden soll.
     * @return Set der ausgewaehlten Beschreibung
     */
    private HashSet<String> waehleBeschreibung(String loesungswort) {
        if(wortliste.get(loesungswort).size() == 1) {
        	
        	/* Es gibt nur eine Beschreibung fuer das Loesungswort */
            return wortliste.get(loesungswort);
        } else {
        	/* Beschreibung zufaellig aussuchen */
        	ArrayList<String> bListe = new ArrayList<String>();
        	bListe.addAll(wortliste.get(loesungswort));
        	HashSet<String> beschreibung = new HashSet<String>();
        	int index = getIndex(loesungswort);
        	beschreibung.add(bListe.get(index));
        	return beschreibung;
        }
    }
    
    /**
     * Prueft ob die Eingegebenen Wortbeschreibungn ok sind
     * 
     * @param beschreibungen Beschreibungslaenge > 0 und <= 60 Zeichen
     * @throws BeschreibungException wenn eine Beschreibung leer oder laenger als 60 Zeichen ist
     */
    private void pruefeBeschreibungen(Set<String> beschreibungen) throws BeschreibungException {
        if(beschreibungen.isEmpty()) {
            throw new BeschreibungException(beschreibungen.size());
        }
        for (Iterator<String> it = beschreibungen.iterator(); it.hasNext();) {
        	
        	/* Prueft die gefundenen Beschreibungen */
            pruefeBeschreibung(it.next());
        }
    }
    
    /**
     * Prueft ob die neue Wortbeschreibung die eingegeben wurde in Ordnung ist
     * 
     * @param beschreibung Laenge > 0 und <= 60 Zeichen
     * @throws BeschreibungException wenn eine Beschreibung leer oder laenger als 60 Zeichen ist
     */
    private void pruefeBeschreibung(String beschreibung) throws BeschreibungException {
        int laenge = beschreibung.length();

        if (laenge == 0 || laenge > 60) {
            throw new BeschreibungException(laenge);
        }
    }
    
    /**
     * Das uebergebene Loesungswort wird ueberprueft, ob es Zeichen enthaelt und notfalls
     * werden die einzelnen Zeichen standardisiert. Alle kleinbuchstaben werden zu
     * Grossbuchstaben und Umlaute werden umgewandelt. Sonderzeichen gelten als
     * Verstoss und werden nicht akzeptiert.
     * 
     * @param loesungswort Das Loesungswort was ueberprueft wird.
     * @return Das standardisiertes Loesungswort, falls moeglich
     * @throws KeinWortException keine Zeichen enthalten
     * @throws SyntaxException  unzulaessige Zeichen
     */
    public String pruefeLoesungswort(String loesungswort) throws KeinWortException, SyntaxException {
        if(loesungswort.length() == 0) {
            throw new KeinWortException("Kein gueltiges Loesungswort");
        }
        loesungswort = loesungswort.toUpperCase();
        loesungswort = loesungswort.replace("Ã", "AE");
        loesungswort = loesungswort.replace("Ö", "OE");
        loesungswort = loesungswort.replace("Ü", "UE");
        loesungswort = loesungswort.replace("ss", "SS");
        loesungswort = loesungswort.replace("0", "O");
        
        for (byte i = 0;i<loesungswort.length();i++) {
            char buchstabe = loesungswort.charAt(i);
            int zeichen = buchstabe;
            
            /* Nur Grossbuchstaben "zwischen" A-Z sind zulaessig
             * sonst fliegt eine SyntaxException */
            if (zeichen < 65 | zeichen > 90) {
                throw new SyntaxException(buchstabe);
            }
        }
        return loesungswort;
    }
    
    /**
     * Gibt eine ArrayList der Woerter zurueck, die den uebergebenen String enthalten sind.
	 * Wird bspw. "ST" eingegeben, so werden alle Loesungswoerter aus der Liste gesucht 
	 * die ein "ST" enthalten. Ist der uebergabeparameter leer, so werden alle Woerter zurueckgegeben.
     *  
     * @param wortsuche String wonach gesucht wird
     * @return gefundene Woerter als ArrayList
     */
    public ArrayList<String> getWortsuche(String wortsuche) {
        ArrayList<String> loesungswoerter = new ArrayList<String>();
        if (wortsuche.isEmpty() == true) {
        	
        	/* Durchlaufe die Woerterliste und uebergebe dem Stringarray 
        	 * alle Loesungswoerter, da das Suchwortfeld leer war */
            for (Iterator<String> it = wortliste.keySet().iterator(); it.hasNext();) {
                loesungswoerter.add(it.next());
            }
        } else {
        	
        	/* Durchlaufe die Woerterliste und uebergebe dem Stringarray 
        	 * nur die Woerter die im Filter eingegeben wurden */
            for (Iterator<String> it = wortliste.keySet().iterator(); it.hasNext();) {
                String wort = it.next();
                if (wort.contains(wortsuche)) {
                    loesungswoerter.add(wort);
                }            
            }
        }

        return loesungswoerter;
    }
    
    /**
     * Ermittelt zufaellig fuer den Generator eine Beschreibung zu ein Wort, 
     * indem ein Index zufaellig aus der Menge der Beschreibungen bestimmt wird.
     * 
     * @param wort	Das Loesungswort
     * @return Der Index einer Beschreibung eines Loesungswortes
     */
    public int getIndex(String wort){
    	int idx = wortliste.get(wort).size();
    	int zufall = (int) (Math.random() * idx) + 0;
    	return zufall;
    }
    
    /**
     * gibt die aktuelle Wortliste zurueck
     * @return Wortliste
     */
    public TreeMap<String, HashSet<String>> getWoerterliste(){
    	return wortliste;
    }
    
    /**
     * Gibt alle Beschreibungen zu einem gegebenen Loesungswort und gibt
     * dieses als ArrayList zurueck. Diese Methode ist fuer das Generieren
     * eines Raetsels da.
     * 
     * @param loesungswort Loesungswort zu dem alle Beschreibungen gesucht werden
     * @throws ArrayIndexOutOfBoundsException Grenzueberschreitung
     * 
     * @return ArrayList mit den Beschreibungen eines Wortes.
     */
    public ArrayList<String> getBeschreibung(String loesungswort) throws ArrayIndexOutOfBoundsException {
        ArrayList<String> beschreibungen = new ArrayList<String>();
        beschreibungen.addAll(wortliste.get(loesungswort));
    	return beschreibungen;
    }
    
    /**
     * Gibt alle Beschreibungen zu einem gegebenen Loesungswort und gibt
     * dieses als HashSet zurueck. Diese Methode wird fuer den Wortbeschreibungsdialog
     * benaetigt, um alle Beschreibungen in die Liste aufzuzeigen.
     * 
     * @param loesungswort Loesungswort zu dem alle Beschreibungen gesucht werden
     * @return HashSet mit allen Wortbeschreibungen zu ein Loesungswort
     */
    public HashSet<String> getBeschreibungenSet(String loesungswort) {
    	HashSet<String> beschreibungen = new HashSet<String>();
    	beschreibungen.addAll(wortliste.get(loesungswort));
        return beschreibungen;
    }
    
    /**
     * hier wird eine Kopie der aktuellen Wortliste erstellt und zurueckgegeben.
     * 
     * @return Kopie der aktuellen Wortliste
     */
    public Wortliste getKopie() {
        Wortliste kopie = new Wortliste();
        ArrayList<String> woerter = new ArrayList<String>();
        
        /* Kopiere die komplette Liste */
        woerter = getWortsuche("");
        for (int i = 0; i < woerter.size(); i++) {
            try {
                kopie.wortHinzufuegen(woerter.get(i), getBeschreibungenSet(woerter.get(i)));
            } catch (Exception e) {}
        }
        return kopie;
    }
    
    /**
     * gibt die Anzahl der Loesungswoerter in der Wortliste zurueck
     * 
     * @return Anzahl der Loesungswoerter.
     */
    public int getAnzahlLoesungswoerter() {
        return wortliste.size();
    }
    
    /**
     * gibt die Laenge des kuerzestens Loesungswortes zurueck
     * 
     * @return Anzahl der Zeichen im kuerzesten Wort.
     */
    public int getKuerzestesWort(){
        int min = Byte.MAX_VALUE;
        for (String w : wortliste.keySet()) {
            if (w.length() < min && w.length() > 1) {
                min = w.length();
            }
        }
        return min;
    }
    
    /**
     * gibt die Laenge des laengsten Loesungswortes zurueck
     * 
     * @return Anzahl der Zeichen im laengsten Wort.
     */
    public int getLaengstesWort() {
        int max = 0;
        for (String w : wortliste.keySet()) {
            if (w.length() > max) {
                max = w.length();
            }
        }
        return max;
    }
    
    /**
     * gibt die Gesamtlaenge aller Woerter in der Wortliste zurueck
     * 
     * @return Gesamte Wortlaenge als Integer
     */
    public int getGesamteWortLaenge() {
        int l = 0;
        for (String w : wortliste.keySet()) {
            l = l + w.length();
        }
        return l;
    }

}
