package steuerung;

/**
 * Ein Datentyp der zwei gleiche Datentypen speichern kann
 *
 * @param <T>	Der Uebergebene Datentyp
 */
public class Tupel<T> {
	
	/** linker Inhalt */
	public T links;
	
	/** rechter Inhalt */
	public T rechts;
	
	/**
	 * Der Konstruktor erzeugt ein Tupel mit unterscheidlichen
	 * Datentypen, um so ein Tupel zu erhalten der flexibler
	 * Einsetzbar ist
	 * 
	 * @param links	ein Datentyp
	 * @param rechts ein Datentyp
	 */
	public Tupel(T links, T rechts){
		this.links = links;
		this.rechts = rechts;
	}
	
	/**
	 * gibt die linke Seite des Tupels zurueck
	 * @return linker Inhalt
	 */
	public T getLinks(){
		return links;
	}
	
	/**
	 * gibt die rechte Seite des Tupels zurueck
	 * @return rechter Inhalt
	 */
	public T getRechts(){
		return rechts;
	}
}
