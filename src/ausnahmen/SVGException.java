package ausnahmen;

/**
 * Wird geworfen, wenn ein Fehler beim
 * speichern auftritt welcher verhindert, dass eine gueltige SVG-Datei
 * erzeugt werden kann wie z.B. fehlende Angabe zur Breite und Hoehe
 */
public final class SVGException extends Exception {
	
	/**
	 * Aufruf des Konstruktors mit dem zu
	 * erscheinenden Fehlertext
	 * 
	 * @param text Fehlertext
	 */
	public SVGException(String text){
		super(text);
	}

}
