package ausnahmen;

/** 
 * Diese Exception wird geworfen, wenn ein Loesungswort was angelegt wird trotz 
 * ersetzungen noch unzulaessige Zeichen enthaelt. Es sind nur Zeichen von A-Z erlaubt,
 * die Gross sein muessen.
 */
public final class SyntaxException extends Exception {
	/**
	 * Aufruf des Konstruktors mit dem zu
	 * erscheinenden Fehlertext
	 * 
	 * @param zeichen das Zeichen was nicht zugelassen ist
	 */
    public SyntaxException(char zeichen) {
        super(String.valueOf(zeichen));
    }
}
