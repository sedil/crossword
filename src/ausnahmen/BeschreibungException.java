package ausnahmen;

/**
 * Diese Exception wird dann geworfen, wenn es keine Wortbeschreibung gibt oder 
 * diese Wortbeschreibung zu lang sein sollte, um Sie sinnvoll in ein
 * Kaestchen darzustellen
 */
 public final class BeschreibungException extends Exception {
	 
	/**
	 * Aufruf des Konstruktors mit dem zu
	 * erscheinenden Fehlertext
	 * 
	 * @param n Anzahl der Buchstaben
	 */
	public BeschreibungException(int n) {
        super(String.valueOf(n));
    }
}
