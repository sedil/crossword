package ausnahmen;

/**
 * Diese Exception wird geworfen, wenn eine Koordinate im Feld besetzt ist
 */
public final class FeldException extends Exception {
	/**
	 * Aufruf des Konstruktors mit dem zu
	 * erscheinenden Fehlertext
	 * 
	 * @param text Fehlertext
	 */
    public FeldException(String text) {
        super(text);
    }
}
