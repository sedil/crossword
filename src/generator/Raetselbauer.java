package generator;

import java.util.Arrays;
import java.util.HashSet;

import steuerung.Tupel;
import ausnahmen.FeldException;

/**
 * Diese Klasse enthaelt Algorithmen, welche versuchen die Positionen 
 * der Loesungswoerter und der zugehoerigen Beschreibungen zu ermittelt, 
 * sowie die Richtung wohin ein Loesungswort verlaufen soll. Diese Informationen 
 * werden als Quintupel als ein eigener Datentyp <code>Feld</code>
 * zusammengefasst in eine Liste gespeichert, sofern ein Wort sicher in das
 * Kreuzwortraetsel eingefuegt werden kann.
 */
public class Raetselbauer {
    /**
     * Diese Matrix enthaelt die einzelnen Buchstaben des Loesungswortes
     */
    private char[][] buchstabenMatrix;
    
    /**
     * Diese Matrix enthaelt die einzelnen Buchstaben, ob ein Feldelement
     * besetzt ist und was dieses Feldelement repraesentiert.</br>
     * Es sind die Werte aus <code>Feldstatus</code> moeglich.
     */
    private char[][] feldBesetztMatrix;
    
    /**
     * Dieses Set enthaelt alle Informationen ueber das Raetsel
     */
    private HashSet<Feld> raetseldatenliste;
    
    /**
     * maximale Breite des Raetsels in Zellen
     */
    private int breite;
    
    /**
     * maximale Hoehe des Raetsels in Zellen
     */
    private int hoehe;
    
    /**
     * Der Konstruktor erzeugt eine initiale Raetselgroesse, indem
     * die Breite und Hoehe uebergeben wird. Ausserdem werden alle
     * 2D-Arrays als 'UNGENUTZT' initialisiert.
     */
    Raetselbauer(int b, int h){
        breite = b;
        hoehe = h;      
        buchstabenMatrix = new char[h][b];
        feldBesetztMatrix = new char[h][b];
        raetseldatenliste = new HashSet<Feld>();
        for (int i=0; i < buchstabenMatrix.length; i++) {
            Arrays.fill(buchstabenMatrix[i], Feldstatus.UNGENUTZT);
            Arrays.fill(feldBesetztMatrix[i], Feldstatus.UNGENUTZT);
        }
    }
    
    /**
     * Fuegt eine Loesungswortbeschreibung neben der angegebenen Startposition ein und ermittelt die 
     * Beschreibungsposition.<code>pruefeObBeschreibungPasst</code> prueft vorher, ob die ermittelte
     * Position frei ist. Es sind insgesamt drei Positionen neben der Startkoordinate moeglich.</br>
     * 
     * Horizontal => Norden, Westen, Sueden</br>
     * Vertikal => Norden, Westen, Osten</br>
     * 
     * @param startkoord Koordinate des Loesungswortes
     * @param richtung Richtung des Loesungswortes (horizontal oder vertikal)
     * @param loesungswort Das Loesungswort
     * @param beschreibung Die Loesungswortbeschreibung
     */
    void beschreibungEinfuegen(Tupel<Integer> startkoord, String loesungswort, char richtung, String beschreibung) throws FeldException,ArrayIndexOutOfBoundsException {
        
    	if (richtung == Feldstatus.HORIZONTAL){
            /* (z,sp-1) = westliche Beschreibungsplatzierung, links des ersten Loesungswortbuchstaben
        	 * (z-1,sp) = noerdliche Beschreibungsplatzierung, oberhalb des ersten Loesungswortbuchstaben
        	 * (z+1,sp) = suedliche Beschreibungsplatzierung, unterhalb des ersten Loesungswortbuchstaben
        	 */
        	
        	/* Pruefe westliche Positionsmoeglichkeit */
    		Tupel<Integer> west = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()-1);
            if (pruefeFeldPosition(west) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, west, beschreibung);
                return;
            }
            
        	/* Pruefe noerdliche Positionsmoeglichkeit */
            Tupel<Integer> nord = new Tupel<Integer>(startkoord.getLinks()-1, startkoord.getRechts());
            if (pruefeFeldPosition(nord) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, nord, beschreibung);
                return;
            }
            
        	/* Pruefe suedliche Positionsmoeglichkeit */
            Tupel<Integer> sued = new Tupel<Integer>(startkoord.getLinks()+1, startkoord.getRechts());
            if (pruefeFeldPosition(sued) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, sued, beschreibung);
                return;
            }
        } else if (richtung == Feldstatus.VERTIKAL) {
        	
        	/* Pruefe noerdliche Positionsmoeglichkeit */
    		Tupel<Integer> nord = new Tupel<Integer>(startkoord.getLinks()-1, startkoord.getRechts());
            if (pruefeFeldPosition(nord) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, nord, beschreibung);
                return;
            }
            
            /* Pruefe westliche Positionsmoeglichkeit */
            Tupel<Integer> west = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()-1);
            if (pruefeFeldPosition(west) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, west, beschreibung);
                return;
            }
            
            /* Pruefe oestliche Positionsmoeglichkeit */
            Tupel<Integer> ost = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()+1);
            if (pruefeFeldPosition(ost) == true) {
                wortEinfuegen(startkoord, loesungswort, richtung, ost, beschreibung);
                return;
            }
        } else {
       	 throw new FeldException("Wort konnte nicht platziert werden");
        }
    } 

    /**
	 * Hier stehen nun alle Informationen zur Verfuegung und es wird letztmalig eine ueberpruefung stattfinden, 
	 * ob alles seine Richtigkeit hat (<code>pruefeObWortPasst</code> und <code>pruefeObBeschreibungPasst</code>).
     * 
     * Fuegt in jeder Position der Buchstabenmatrix ein Buchstabe ein, in Abhaengigkeit, ob
     * das Loesungswort horizontal oder vertikal verlaeuft. Das Ergebnis ist eine
     * Anreihung von Buchstaben was das Loesungswort zu einer bestimmten Wortbeschreibung darstellt.
     * 
     * <ul> Beispiel : feldBesetztMatrix[1][1] = VERTIKAL; bedeutet, dass bei dieser Position
     * ein vertikales Loesungswort von nord nach sued verlaeuft.
     * 
     * und buchstabenMatrix[0][1] = 'B' , buchstabenMatrix[0][2] = 'A' , buchstabenMatrix[0][3] = 'U' , buchstabenMatrix[0][4] = 'M'
     * waere ein Beispiel wie ein Loesungswort dargestellt wird. </ul>
     * 
     * @param startkoord Startkoordinate des Loesungswortes
     * @param richtung Die Richtung des Loesungswortes
     * @param loesungswort Das Loesungswort
     * @param beschreibungkoord Position der Wortbeschreibung
     * @param beschreibung Beschreibung des Loesungswortes
     * @throws FeldException Feld ist besetzt
     * @throws ArrayIndexOutOfBoundsException Grenzen ueberschritten
     */
    private void wortEinfuegen(Tupel<Integer> startkoord, String loesungswort, char richtung, Tupel<Integer> beschreibungkoord, String beschreibung) throws FeldException, ArrayIndexOutOfBoundsException {
        int zeile = startkoord.getLinks();
        int spalte = startkoord.getRechts();
        if (richtung == Feldstatus.VERTIKAL) {
            /* Das Loesungswort darf nicht ausserhalb des Raetselgitters beginnen */
            if (zeile-1 >= 0) {
            	
            	/* Ungenutzte Felder sind leer und koennten verwendet werden */
                if (feldBesetztMatrix[zeile-1][spalte] == Feldstatus.UNGENUTZT ) {
                    feldBesetztMatrix[zeile-1][spalte] = Feldstatus.LEER;
                }
            }
            
            /* Das Loesungswort darf nicht ausserhalb des Raetselgitters enden */
            if (zeile + loesungswort.length() < hoehe) {
            	
            	/* Ungenutzte Felder sind leer */
                if (feldBesetztMatrix[zeile+loesungswort.length()][spalte] == Feldstatus.UNGENUTZT ) {
                    feldBesetztMatrix[zeile+loesungswort.length()][spalte] = Feldstatus.LEER;
                }
            }
            
            for (short dz = 0 ; dz < loesungswort.length(); dz++) {
                int aktZeile = dz + zeile;
                
                /* Kreuzt sich ein vertikales und ein horizontales Element, dann
                 * kann dort eine Wortkreuzung entstehen */
                if (feldBesetztMatrix[aktZeile][spalte] == Feldstatus.HORIZONTAL) {
                    feldBesetztMatrix[aktZeile][spalte] = Feldstatus.BEIDE;
                } else {
                	/* Keine Wortkreuzung, sondern nur ein vertikales Wort */
                    feldBesetztMatrix[aktZeile][spalte] = Feldstatus.VERTIKAL;
                }
                
                /* Buchstabe des Loesungswortes in die Matrix einfuegen */
                buchstabenMatrix[aktZeile][spalte] = loesungswort.charAt(dz);
            }
        } else if (richtung == Feldstatus.HORIZONTAL){
            if (spalte-1 >= 0) {
                if (feldBesetztMatrix[zeile][spalte-1] == Feldstatus.UNGENUTZT ) {
                    feldBesetztMatrix[zeile][spalte-1] = Feldstatus.LEER;
                }
            }
            if (spalte + loesungswort.length() < breite) {
                if (feldBesetztMatrix[zeile][spalte+loesungswort.length()] == Feldstatus.UNGENUTZT ) {
                    feldBesetztMatrix[zeile][spalte+loesungswort.length()] = Feldstatus.LEER;
                }
            }
            
            for (int ds = 0 ; ds < loesungswort.length(); ds++) {
                int aktSpalte = ds + spalte;
                if (feldBesetztMatrix[zeile][aktSpalte] == Feldstatus.VERTIKAL) {
                    feldBesetztMatrix[zeile][aktSpalte] = Feldstatus.BEIDE;
                } else {
                    feldBesetztMatrix[zeile][aktSpalte] = Feldstatus.HORIZONTAL;
                }
                buchstabenMatrix[zeile][aktSpalte] = loesungswort.charAt(ds);
            }

        }
        
        /* Diese Position ist fuer die Wortbeschreibung */
        feldBesetztMatrix[beschreibungkoord.getLinks()][beschreibungkoord.getRechts()] = Feldstatus.BESCHREIBUNG;
        
        /* Alle Informationen zusammenfassen */
        Feld d = new Feld(startkoord, loesungswort, richtung, beschreibungkoord, beschreibung);       
        raetseldatenliste.add(d);
    }
    
    /**
     * ermittelt, ob ein Loesungswort in der ermittelten Startkoordinate in das 
     * Raetsel eingefuegt werden kann, ansonsten wird eine FeldException
     * geworfen. Fuer die ueberpruefung wird eine Zeile/Spalte solange durchwandert, wie
     * lange ein Loesungswort ist, um so konflikte zu finden. Es wird geprueft, ob die uebergebene Position</br>
     * 
     * - bereits besetzt ist </br>
     * - ob eine Wortkreuzung ermoeglicht werden kann </br>
     * - ob das Loesungswort ausserhalb des Raetselfeldes beginnt </br>
     * - ob das Loesungswort ausserhalb des Raetselfeldes verlaufen wuerde </br>
     * 
     * Sollte eine Position nicht passen, dann kann das aktuelle Wort dort nicht platziert werden.
     * 
     * @param startpunkt Startposition des Wortes
     * @param richtung Richtung des Wortes
     * @param loesungswort Das Loesungswort
     * 
     * @return true, wenn es keine Konflikte gibt (Wort passt), sonst false
     */
    boolean pruefeObWortPasst(Tupel<Integer> startpunkt, String loesungswort, char richtung) {
        try {       	
            int zeile = startpunkt.getLinks();
            int spalte = startpunkt.getRechts();           
            if (richtung == Feldstatus.VERTIKAL) {  
                /* Das Loesungswort darf nicht ausserhalb der noerdlichen Grenze beginnen */
                if (zeile-1 >= 0) {
                	/* Die zu pruefende Position darf nicht besetzt sein */
                    if (feldBesetztMatrix[zeile-1][spalte] == Feldstatus.VERTIKAL || feldBesetztMatrix[zeile-1][spalte] == Feldstatus.HORIZONTAL || feldBesetztMatrix[zeile-1][spalte] == Feldstatus.BEIDE) {
                    	throw new FeldException("in Feld "+(zeile-1)+","+spalte);
                    }
                } 
                
                /* Wenn das Loesungswort innerhalb der Grenzen bleibt */
                if (zeile + loesungswort.length() < hoehe) {
                	/* Die zu prï¿½fende Position darf nicht besetzt sein */
                    if (feldBesetztMatrix[zeile + loesungswort.length()][spalte] == Feldstatus.VERTIKAL || feldBesetztMatrix[zeile + loesungswort.length()][spalte] == Feldstatus.HORIZONTAL || feldBesetztMatrix[zeile + loesungswort.length()][spalte] == Feldstatus.BEIDE) {
                    	throw new FeldException("in Feld "+(zeile+loesungswort.length())+","+spalte+" : "+loesungswort);
                    } 
                }
            	
                for (short dz = 0 ; dz < loesungswort.length(); dz++) {
                    int aktZeile = dz + zeile;
                    
                    /* Wenn diese Feldposition schon besetzt oder bereits Vertikal ist, so kann hier nichts 
                     * weiteres platziert werden */
                    if (!(feldBesetztMatrix[aktZeile][spalte] == Feldstatus.UNGENUTZT || feldBesetztMatrix[aktZeile][spalte] == Feldstatus.HORIZONTAL)) {
                        throw new FeldException("in Feld "+aktZeile+","+spalte+" : "+loesungswort);
                    } 

                    /* Pruefe, ob der Buchstabe des Loesungswortes an der Stelle 'aktZeile' mit dem Buchstaben der Datenmatrix an derselben
                     * Stelle (aktZeile,spalte) uebereinstimmt, um eine Wortkreuzung zu ermoeglichen. Wenn nicht, dann wird eine 
                     * FeldException geworfen. */
                    if (feldBesetztMatrix[aktZeile][spalte] == Feldstatus.HORIZONTAL && buchstabenMatrix[aktZeile][spalte] != loesungswort.charAt(dz)) {
                    	throw new FeldException("in Feld "+aktZeile+","+spalte+" : "+loesungswort);
                    }
                }
                
            } else if (richtung == Feldstatus.HORIZONTAL){
                if (spalte-1 >= 0) {
                    if (feldBesetztMatrix[zeile][spalte-1] == Feldstatus.VERTIKAL || feldBesetztMatrix[zeile][spalte-1] == Feldstatus.HORIZONTAL || feldBesetztMatrix[zeile][spalte-1] == Feldstatus.BEIDE) {
                        throw new FeldException("in Feld "+zeile+","+(spalte-1)+" : "+loesungswort);
                    } 
                }
                if (spalte + loesungswort.length() < breite) {
                    if (feldBesetztMatrix[zeile][spalte + loesungswort.length()] == Feldstatus.VERTIKAL || feldBesetztMatrix[zeile][spalte + loesungswort.length()] == Feldstatus.HORIZONTAL || feldBesetztMatrix[zeile][spalte + loesungswort.length()] == Feldstatus.BEIDE) {
                        throw new FeldException("in Feld "+zeile+","+(spalte+loesungswort.length())+" : "+loesungswort);
                    }
                }  
            	
                for (short ds = 0 ; ds < loesungswort.length(); ds++) {
                    int aktSpalte = ds + spalte;

                    if (!(feldBesetztMatrix[zeile][aktSpalte] == Feldstatus.UNGENUTZT || feldBesetztMatrix[zeile][aktSpalte] == Feldstatus.VERTIKAL)) {
                        throw new FeldException("in Feld "+zeile+","+aktSpalte+" : "+loesungswort);
                    } 
                    
                    if (feldBesetztMatrix[zeile][aktSpalte] == Feldstatus.VERTIKAL && buchstabenMatrix[zeile][aktSpalte] != loesungswort.charAt(ds)) {
                        throw new FeldException("in Feld "+zeile+","+aktSpalte+" : "+loesungswort);
                    }
                }        
            }
        } catch (FeldException fe) {
            return false;
        } catch (ArrayIndexOutOfBoundsException ae){
        	return false;
        }
        return true;
    }
       
    /**
     * Diese Methode prueft, ob das uebergebene Loesungsowrt Buchstaben mit
     * bereits vorhandenen Loesungswoertern gemeinsam hat. Dies ist definitiv der
     * Fall, wenn eine Wortkreuzung ermoeglicht werden soll.
     * 
     * @param startpunkt Startkoordinate des Loesungswortes
     * @param richtung Die Richtung des Loesungswortes
     * @param loesungswort Das Loesungswort
     * @return gemeinsame Zahl an Buchstaben als Integer<br>
     * <br> 0 = keine Gemeinsamkeit, keine Wortkreuzung moeglich
     * <br> >0 = mindestens eine Wortkreuzung fuer das aktuelle Wort moeglich
     */
    int pruefeZeichenGemeinsamkeit(Tupel<Integer> startpunkt, String loesungswort, char richtung) {
        int treffer = 0;
        int zeile = startpunkt.getLinks();
        int spalte = startpunkt.getRechts();
        if (richtung == Feldstatus.VERTIKAL) {
        	
        	/* Zaehle wieviele Buchstaben ein Loesungswort und wieviele 'h'
        	 * die feldBesetztMatrix haben. Dadurch kann ein
        	 * anderes Loesungswort gleicher laenge eingesetzt werden */
            for (short dz = 0 ; dz < loesungswort.length(); dz++) {
                int aktZeile = dz + zeile;
                if (feldBesetztMatrix[aktZeile][spalte] == Feldstatus.HORIZONTAL) {
                    treffer++;
                }
            }
        } else if (richtung == Feldstatus.HORIZONTAL){
            for (short ds = 0 ; ds < loesungswort.length(); ds++) {
                int aktSpalte = ds + spalte;
                if (feldBesetztMatrix[zeile][aktSpalte] == Feldstatus.VERTIKAL) {
                    treffer++;
                }
            }
        }
        return treffer;
    }
    
    /**
     * prueft, ob an der uebergebenen Wortbeschreibungskoordinate eine Beschreibung 
     * eingefuegt werden kann. Dabei wird geprueft, ob diese Position noch frei oder leer ist.
     * 
     * @param bPos Beschreibungsposition
     * @throws FeldException Feld ist besetzt
     * @throws ArrayIndexOutOfBoundsException Grenzen ueberschritten
     */
    boolean pruefeFeldPosition(Tupel<Integer> bPos) {
    	int zeile = bPos.getLinks();
    	int spalte = bPos.getRechts();
    	
    	try {
    		if(feldBesetztMatrix[zeile][spalte] == Feldstatus.UNGENUTZT || feldBesetztMatrix[zeile][spalte] == Feldstatus.LEER){
    			return true;
    		} else {
    			 throw new FeldException("in Feldposition ("+zeile+" "+spalte+")");
    		}
    	} catch (ArrayIndexOutOfBoundsException ae){
    		return false;
    	} catch (FeldException fe){
    		return false;
    	}
    }
    
    /**
     * Anhand der uebergebenen Startkoordinate eines Wortes wird ueberprueft in welche Richtung 
     * ein Loesungswort von dort aus beginnen kann. Um mehr Beschreibungen in der ersten Zeile/Spalte zu
     * ermoeglichen, wird im Vorfeld ausgeschlossen, dass dort ein horizontales bzw. vertikales
     * Loesungsort beginnt.
     * 
     * Mithilfe der Eintraege der <code>feldBesetztMatrix</code> wird entschieden, wie ein
     * Loesungswortes eingefuegt werden kann und in welcher Richtung das Loesungswort verlaufen kann. Beim ersten
     * Durchlauf sind die Eintraege allerdings 'UNGENUTZT', da die Feldelemente so initialisiert wurden.</br>
     * 
     * Beispiel :
     * <br>[b][l][b][v][l]
     * <br>[h][h][+][h][h]
     * <br>[l][l][v][l][l]
     * <br>[l][l][v][l][l]
     * 
     * @param startpunkt Startkoordinate, die von <code>erzeugeRaetsel</code> stammt
     * @return Die Richtung, wohin das Loesungswort verlaufen kann.
     */
    char pruefeMoeglicheStartrichtung(Tupel<Integer> startpunkt) {
        boolean vertikal = false;
        boolean horizontal = false;

        switch (feldBesetztMatrix[startpunkt.getLinks()][startpunkt.getRechts()]) {     
        case Feldstatus.HORIZONTAL : {
        	
        	/* Ein Wort kann vertikal von oben nach unten
        	 * eingefuegt werden, wobei sich an dieser Position schon ein
        	 * Buchstabe eines bereits horizontal eingefuegten Wort befindet */
            if (startpunkt.getRechts() > 0 & pruefeWoEineBeschreibungPasst(startpunkt, Feldstatus.VERTIKAL) == true) {
                vertikal = true;
            }
            break;
        }
        case Feldstatus.VERTIKAL : {

            if (startpunkt.getLinks() > 0 & pruefeWoEineBeschreibungPasst(startpunkt, Feldstatus.HORIZONTAL) == true) {
                horizontal = true;
            }
            break;
        }
        
        /* Am Anfang ist jedes Feldelement noch 'UNGENUTZT', da es so initialisiert wurde */
        case Feldstatus.UNGENUTZT : {
            if (startpunkt.getRechts() > 0 & pruefeWoEineBeschreibungPasst(startpunkt, Feldstatus.VERTIKAL) == true) {
                vertikal = true;
            }
            if (startpunkt.getLinks() > 0 & pruefeWoEineBeschreibungPasst(startpunkt, Feldstatus.HORIZONTAL) == true) {
                horizontal = true;
            }
            break; 
        }
        
        case Feldstatus.BESCHREIBUNG : {
        	return Feldstatus.NICHTS; // Feld enthaelt eine Beschreibung
        }
        case Feldstatus.BEIDE		 : {
        	return Feldstatus.NICHTS; // eine Wortkreuzung
        }
        case Feldstatus.LEER		 : {
        	return Feldstatus.NICHTS; // leeres Feld
        }}
        
        /* Gebe den Wert zurueck, in welche Richtung ein Wort gelegt werden kann */
        if (vertikal == true & horizontal == false) {
        	return Feldstatus.VERTIKAL;
        }
        if (vertikal == false & horizontal == true) {
        	return Feldstatus.HORIZONTAL;
        }
        if (vertikal == true & horizontal == true)  {
        	return Feldstatus.BEIDE;
        }
        
        return Feldstatus.NICHTS;
    }
    
    /**
     * ueberprueft, ob zu der uebergebenden Startkoordinate eine passende freie Position fuer eine
     * Beschreibung existiert. So eine Beschreibung kann im Falle eines horizontalen
     * Wortes nur oberhalb, unterhalb oder links der uebergebenen Koordinate und im Falle
     * eines vertikalen Wortes nur links, oberhalb oder rechts der Koordinate existieren.
     * 
     * @param startkoord Startposition des Loesungswortes
     * @param richtung   Richtung des Loesungswortes
     * @return true, wenn eine passende Position fuer die Wortbeschreibung exisitert, sonst false
     */
    private boolean pruefeWoEineBeschreibungPasst(Tupel<Integer> startkoord, char richtung) {
        if (richtung == Feldstatus.VERTIKAL) {
        	
        	/* Pruefe, ob noerdlich der Startposition eine Beschreibung
        	 * platziert werden kann */
        	Tupel<Integer> norden = new Tupel<Integer>(startkoord.getLinks()-1, startkoord.getRechts());
            if (pruefeFeldPosition(norden) == true) {
            	return true;
            }
            
        	/* Pruefe, ob westlich der Startposition eine Beschreibung
        	 * platziert werden kann */
            Tupel<Integer> westen = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()-1);
            if (pruefeFeldPosition(westen) == true) {
            	return true;
            }
            
        	/* Pruefe, ob oestlich der Startposition eine Beschreibung
        	 * platziert werden kann */
            Tupel<Integer> osten = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()+1);
            if (pruefeFeldPosition(osten) == true){ 
            	return true;
            }
        } else if ( richtung == Feldstatus.HORIZONTAL){
        	
        	/* Pruefe, ob westlich der Startposition eine Beschreibung
        	 * platziert werden kann */
        	Tupel<Integer> westen = new Tupel<Integer>(startkoord.getLinks(), startkoord.getRechts()-1);
            if (pruefeFeldPosition(westen) == true){ 
            	return true;
            }
            
        	/* Pruefe, ob suedlich der Startposition eine Beschreibung
        	 * platziert werden kann */
            Tupel<Integer> norden = new Tupel<Integer>(startkoord.getLinks()-1, startkoord.getRechts());
            if (pruefeFeldPosition(norden) == true) {
            	return true;
            }
            
        	/* Pruefe, ob noerdlich der Startposition eine Beschreibung
        	 * platziert werden kann */
            Tupel<Integer> sueden = new Tupel<Integer>(startkoord.getLinks()+1, startkoord.getRechts());
            if (pruefeFeldPosition(sueden) == true){ 
            	return true;
            }
        }
        return false;
    }
    
    /**
     * ermittelt anhand der neu ermittelten Feldgrenzen ein kleinstmoegliches Raetsel,
     * um so auch die Anzahl der Grauen Felder zu reduzieren, sie verbrauchen nur
     * unnoetigen Platz.
     * @return kleinstes Raetsel
     */
    Raetselbauer getKleinstesRaetsel(Tupel<Integer> grenze) {
    	int neueHoehe = grenze.getLinks();
    	int neueBreite = grenze.getRechts();
        if (neueBreite < breite || neueHoehe < hoehe) {
        	
        	/* Erzeuge das gekuerzte neue WortFeld, falls es das gibt */
            Raetselbauer min = new Raetselbauer(neueBreite, neueHoehe);
            for (Feld f : raetseldatenliste) {
                try {
                	min.wortEinfuegen(f.getStartKoordinate(),f.getLoesungswort(),f.getRichtung(),
                					  f.getBeschreibungKoordinate(),f.getBeschreibung());
                }
                catch (Exception e) {}
            }
            return min;
        } else {
        	/* kein kleineres Raetsel moeglich */
            return this;
        }
    }  
    
    /**
     * ermittelt, welche Zeilen und Spalten leer sind, um so die neue
     * Raetselgroesse zu erhalten.
     * @return neue Raetselgroesse
     */
    Tupel<Integer> getNeueGrenzen(){
        int x = hoehe - 1;
        boolean benutztesFeldGefunden = false;
        for (x = hoehe - 1; x >= 0; x--) {
            for (short dy = 0; dy < breite; dy++) {
            	
            	/* Wenn es Felder gibt die besetzt sind, dann breche ab. 
            	 * Es kann nichts gekuerzt werden */
                if (!(feldBesetztMatrix[x][dy] == Feldstatus.UNGENUTZT || feldBesetztMatrix[x][dy] == Feldstatus.LEER)) {
                    benutztesFeldGefunden = true;
                    break;
                }
            }
            if (benutztesFeldGefunden == true) break;
        }
        
        /* Unbenutzte Spalten koennen entfernt werden, daher neue Hoehe */
        int neueHoehe = x + 1;
        benutztesFeldGefunden = false;
        int y = breite - 1;
        for (y = breite - 1; y >= 0; y--) {
            for (short dx = 0; dx < hoehe; dx++) {
                if (!(feldBesetztMatrix[dx][y] == Feldstatus.UNGENUTZT || feldBesetztMatrix[dx][y] == Feldstatus.LEER)) {
                    benutztesFeldGefunden = true;
                    break;
                }
            }
            if (benutztesFeldGefunden == true) break;
        }
        
        /* Unbenutzte Zeilen koennen entfernt werden */
        int neueBreite = y + 1;
    	return new Tupel<Integer>(neueHoehe,neueBreite);
    }
    
    /**
     * gibt das Raetsel als HashSet zurueck, wo alle benoetigten
     * Informationen vorhanden sind
     * @return Raetsel
     */
    public HashSet<Feld> getRaetsel(){
    	return raetseldatenliste;
    }
    
    /**
     * gibt das 2D-Array mit Buchstaben zurueck, wie ein Feldelement besetzt ist
     * @return Status eines Felded
     */
    public char[][] getFeldBesetztMatrix(){
    	return feldBesetztMatrix;
    }
    
    /**
     * gibt die Loesung des Raetsels zuuerck
     * @return Loesung des Raetsels
     */
    public char[][] getBuchstabenMatrix(){
    	return buchstabenMatrix;
    }
    
    /**
     * gibt die Breite des Raetsels zurueck
     * @return Breite
     */
    public int getBreite(){
    	return breite;
    }
    
    /**
     * gibt die Hoehe des Raetsels zurueck
     * @return Hoehe
     */
    public int getHoehe(){
    	return hoehe;
    }
}
