package generator;

import steuerung.Tupel;

/**
 * In dieser Klasse werden alle relevanten Daten fuer ein Wort und dessen 
 * Beschreibung, sowie die Richtung innerhalb eines Raetselfeldes zusammengefasst.
 * Mit diesen Informationen kann ein Wort in das Raetsel eingefuegt werden, und
 * auch grafisch ausgegeben werden
 */
public final class Feld {
    /**
     * Das Loesungswort
     */
    private String loesungswort;
    
    /**
     * Die Loesunswortbeschreibung
     */
    private String beschreibung;
    
    /**
     * Die Startposition des Loesungswortes, also wo der erste
     * Buchstabe des Loesungswortes steht.
     */
    private Tupel<Integer> startposition;
    
    /** 
     * Position der Loesungswortbeschreibung als Koordinate wo
     * die Wortbeschreibung steht
     */
    private Tupel<Integer> beschreibungPosition;
    
    /**
     * Richtung des Loesungswortes, entweder verlÃ¤uft das
     * Loesungswort in horizontale Richtung oder in
     * vertikale Richtung
     */
    private char richtung;
    
    /**
     * Der Konstruktor. Er fasst alle noetigen Informationen ueber ein
     * Feldelement als ein Quintupel zusammen
     * 
     * @param start Startposition des ersten Buchstaben des Loesungswortes, wobei der erste Wert die Zeile, 
     * also die y-Koordinate und der zweite Wert die Spalte, x-Koordinate entspricht
     * @param richtung Richtung des Loesungswortes (horizontal oder vertikal)
     * @param loesungswort Das Loesungswort
     * @param bPos Position der Beschreibung fuer ein Loesungswort. Auch hier ist der erste Wert die Zeile und der
     * zweite Wert die Spalte
     * @param b Die Beschreibung des Loesungswortes
     */
    public Feld(Tupel<Integer> start, String loesungswort, char richtung, Tupel<Integer> bPos, String b) {
        this.startposition = start;
        this.loesungswort = loesungswort;
        this.richtung = richtung;
        this.beschreibungPosition = bPos;
        this.beschreibung = b;
    }
    
    /**
     * gibt die Startkoordinate eines Loesungswortes zurueck, wobei
     * der erste Eintrag die Zeile und der zweite Eintrag
     * die Spalte entspricht.
     * 
     * @return	Startkoordinate
     */
    public Tupel<Integer> getStartKoordinate(){
    	return startposition;
    }
    
    /**
     * gibt das Loesungswort zurueck
     * 
     * @return Loesungswort
     */
    public String getLoesungswort(){
    	return loesungswort;
    }
    
    /**
     * gibt die Verlaufsrichtung des Loesungswortes zurueck
     * 
     * @return	horizontal oder vertikal als h oder v
     */
    public char getRichtung(){
    	return richtung;
    }
    
    /**
     * gibt die Koordinate einer Wortbeschreibung zurueck, wobei
     * der erste Eintrag die Zeile und der zweite Eintrag
     * die Spalte entspricht
     * 
     * @return	Koordinate wo die Wortbeschreibung steht
     */
    public Tupel<Integer> getBeschreibungKoordinate(){
    	return beschreibungPosition;
    }
    
    /**
     * gibt die Loesungswortbeschreibung zurueck
     * 
     * @return Wortbeschreibung
     */
    public String getBeschreibung(){
    	return beschreibung;
    }
}
