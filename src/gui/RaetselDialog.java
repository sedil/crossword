package gui;

import generator.Raetselbauer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import steuerung.Tupel;

/**
 * Dieses Fenster zeigt das generierte Raetsel und gibt den Benutzer mehrere
 * Auswahlmoeglichkeiten, ob er die Loesung des Raetsels sehen moechte, ob er
 * das Raetsel speichern moechte oder drucken moechte.
 */
public class RaetselDialog extends JDialog {
	
	/** Enthaelt das Raetsel */
    private Raetselbauer kreuzwortdaten;
    
    /** Zeichenflaeche des Raetsels */
    private Raetselzeichner raetsel;
    private Dimension standardgroesse;
    
    private File datei;
    
    /** Feld enthaelt JMenuItems, wobei 0 = bild speichern, 1 = loesung anzeigen , 2 = beenden */
    private JMenuItem[] menufeld = new JMenuItem[3];
    
    /**
     * Der Konstruktor baut das Fenster auf und bekommt als Parameter
     * das fertige Kreuzwortraetsel uebermittelt.
     * @param raetsel Das fertige Raetsel
     * @param frame damit nicht mehrere Fenster gleichzeitig bedient werden
     */
    public RaetselDialog(JFrame frame, Raetselbauer raetsel) {
        super(frame, "Kreuzwortraetsel", false);
        kreuzwortdaten = raetsel;
        standardgroesse = new Dimension(1024,768);
        initKomponenten();
        erzeugeMenuBar();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(getAufloesung().getLinks(),getAufloesung().getRechts());
        
        Kreuzwortlistener listener = new Kreuzwortlistener();
        listenerInit(listener);
    }
    
    /**
     * Ordnet jeder Swing-Komponente ein <code>ActionListener</code>-Objekt zu
     * 
     * @param <code>ActionListener</code>-Objekt
     */
    private void listenerInit(ActionListener action) {
    	for(byte i = 0; i < menufeld.length; i++){
    		menufeld[i].addActionListener(action);
    	}
    }
    
    /**
     * initialisiert die JMenuItems
     */
    private void initKomponenten(){
    	menufeld[0] = new JMenuItem("Raetsel speichern");
    	menufeld[1] = new JMenuItem("Loesung anzeigen");
    	menufeld[2] = new JMenuItem("Beenden");
    }
    
    /**
     * baue das Menue fuer das Fenster auf
     */
    private void erzeugeMenuBar() {
        JMenuBar menubar = new JMenuBar();
        JMenu kreuzwortMenu = new JMenu("Raetsel");
        menubar.add(kreuzwortMenu);
        kreuzwortMenu.add(menufeld[0]);
        kreuzwortMenu.add(menufeld[1]);
        kreuzwortMenu.add(menufeld[2]);
        setJMenuBar(menubar);
    }
    
    /**
     * baut das Fenster auf
     */
    private void erzeugeFenster() {
        setLayout(new BorderLayout());
        
        /* Raetsel wird grafisch dargestellt */
        raetsel = new Raetselzeichner(kreuzwortdaten);
        JScrollPane scrollPane = new JScrollPane(raetsel);
        add(scrollPane, BorderLayout.CENTER);
        Dimension raetselgroesse = raetsel.getPreferredSize();
        Dimension bildgroesse = standardgroesse;
        scrollPane.setPreferredSize(new Dimension(Math.min(raetselgroesse.width, bildgroesse.width),
                                                  Math.min(raetselgroesse.height, bildgroesse.height)));
    }
    
    /**
     * ruft die Methode auf, die das Raetsel als *.png-Datei speichert
     */
    private void speichereBild() {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG-Datei", "png");
        final JFileChooser fc;
        if (datei == null) {
            fc = new JFileChooser();
        } else {
            fc = new JFileChooser(datei.getParentFile());
        }   
        fc.addChoosableFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        int r = fc.showSaveDialog(RaetselDialog.this);
        if (r == JFileChooser.APPROVE_OPTION) {
        	File datei = fc.getSelectedFile();
        	datei = new File(datei.getAbsolutePath() + ".png");
            try {
                raetsel.speicherRaetsel(datei);
                JOptionPane.showMessageDialog(null, "Grafik wurde erfolgreich in\n"+datei.getAbsolutePath()+"\ngespeichert", "Speichern", JOptionPane.INFORMATION_MESSAGE);
            } 
            catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Grafik konnte nicht gespeichert werden: " + e.getMessage(), "Speichern", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * Regelt, ob die Loesung des Raetsels angezeigt werden soll
     */
    private void switchLoesungAnzeigen(){
    	if(raetsel.getZeigeLoesung() == true){
    		raetsel.setZeigeLoesung(false);
    	} else if (raetsel.getZeigeLoesung() == false){
    		raetsel.setZeigeLoesung(true);
    	}       	
    	raetsel.repaint();
    }
    
    /**
     * beendet diesen Dialog und schliesst das Fenster
     */
    private void beenden() {
        setVisible(false);
        dispose();
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * Controller fuer die JMenuItems, damit diese ihre Funktionalitaeten
     * erhalten.
     */
    private class Kreuzwortlistener implements ActionListener {

        public void actionPerformed(ActionEvent action) {
            if (action.getSource() == menufeld[0]) {
                speichereBild();
            } else if (action.getSource() == menufeld[1]) {
                switchLoesungAnzeigen();
            } else if (action.getSource() == menufeld[2]) {
                beenden();
            }
        }
    }
}
