package gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import steuerung.Tupel;
import steuerung.Wortliste;

/**
 * Hier hat der Benutzer die Moeglichkeit ein Raetsel per Zufall zu generieren.
 * Dazu wird dieses Fenster zur Verfuegung gestellt. Es wird nur nach Anzahl
 * der Pflichtwoerter und Anzahl der Fuellwoerter gefragt.
 */
public class GenZufallDialog extends JDialog {  
	
	/**
	 * Die komplette Wortliste die alle Woerter enthaelt
	 */
    private Wortliste wortliste;

    /**
     *  ausgewaehlte Loesungswoerter werden sueaeter hier eingefuegt 
     */
    private Wortliste wortAusgewaehlt = new Wortliste();
	
    private JButton ok,beenden;
    private JLabel maxwort, wortinliste, wortzahl, fwortzahl;
    private JTextField wort,fwort;
    
    /** Die Mange an Pflichtwoerter */
    private short wortAnzahlZufall;
    
    /** Die Menge der Fuellwoerter */
    private short fuellwoerter;
    
    /**
     * ob ein Kreuzwortraetsel generiert werden soll
     */
    private boolean sollGenerieren = false;

    /**
     * Der Konstruktor baut das Fenster mit der uebergebenen Wortliste auf
     * 
     * @param frame  Damit keine zwei Fenster gleichzeitig bedient werden
     * @param wListe Wortliste, aus der das Kreuzwortraetsel generiert werden soll
     */
    public GenZufallDialog(JFrame frame, Wortliste wListe) {
        super(frame, "Zufall-Modus", true);
        wortliste = wListe.getKopie();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(getAufloesung().getLinks(),getAufloesung().getRechts());
        
        GenListener listener = new GenListener();
        listenerInit(listener);
    }
    
    /**
     * Ordnet einen <code>ActionListener</code>-Objekt den Swing-Komponenten zu
     * 
     * @param action <code>ActionListener</code>-Objekt
     */
    private void listenerInit(ActionListener action) {
        ok.addActionListener(action);
        beenden.addActionListener(action);
        fwort.addActionListener(action);
        wort.addActionListener(action);
    }
    
    /**
     * initialisiert die ganzen Swing-Komponenten
     */
    private void initKomponenten(){
        ok   = new JButton("Bestaetigen");
        beenden = new JButton("Abbrechen");
        wort = new JTextField(5);
        fwort = new JTextField(5);
        maxwort = new JLabel("Woerter in der Liste");
        wortinliste = new JLabel(Integer.toString(wortliste.getAnzahlLoesungswoerter()));
        wortzahl = new JLabel("Wortanzahl");
        fwortzahl = new JLabel("Fuellwoerter");
    }
    
    /**
     * erzeugt die Fensterdarstellung
     */
    private void erzeugeFenster() {
    	initKomponenten();   	
        setLayout(new BorderLayout());
        erzeugePanel();            
    }

    /**
     * erzeugt das Panel des Fensters
     * 
     * @param panel Panel mit ein Layoutmanager
     */
    private void erzeugePanel() {
    	JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(maxwort, gbc);
        gbc.gridx++;
        panel.add(wortinliste, gbc);
        gbc.gridx--;
        gbc.gridy++;
        panel.add(wortzahl, gbc);
        gbc.gridx++;
        panel.add(wort, gbc);
        wort.setText("0");
        gbc.gridx--;
        gbc.gridy++;
        panel.add(fwortzahl, gbc);
        gbc.gridx++;
        panel.add(fwort, gbc);
        fwort.setText("0"); 
        gbc.gridx--;
        gbc.gridy++;
        panel.add(ok, gbc);
        gbc.gridx++;
        panel.add(beenden,gbc);
        add(panel, BorderLayout.CENTER);
    }
    
    /**
     * bevor der Dialog beendet wird, ueberpruefe die Eingaben
     * 
     * @throws NumberFormatException keine Zahl eingegeben
     */
    private void bestaetigen() throws NumberFormatException {
    	try {
    		wortAnzahlZufall = Short.parseShort(wort.getText());
    		if (wortAnzahlZufall > wortliste.getAnzahlLoesungswoerter()){
    			JOptionPane.showMessageDialog(null, "Nicht genug Woerter in der Wortliste vorhanden", "", JOptionPane.ERROR_MESSAGE);                        
    			return;
    		} else if (wortAnzahlZufall <= 0) {
                JOptionPane.showMessageDialog(null, "Es muss mindestens 1 Wort ausgewaehlt werden", "", JOptionPane.ERROR_MESSAGE);                        
                return;
    		}
        } catch (NumberFormatException e1) {
            wort.setText("0");
            JOptionPane.showMessageDialog(null, "Anzahl muss numerisch sein.", "Anzahl", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException();
        }
    	 
        /* Suche zufaellig Woerter aus der Wortliste raus */
    	Tupel<Wortliste> ergebnis = wortliste.waehleWoerter(wortAnzahlZufall);
        wortliste = ergebnis.getLinks();
        wortAusgewaehlt = ergebnis.getRechts();            
        sollGenerieren = true;
        setVisible(false);
        
        /* Fuellwoerter */
        try {
            fuellwoerter = Short.parseShort(fwort.getText());
            
            if ( fuellwoerter < 0){
           	 JOptionPane.showMessageDialog(null, "Zahl muss groessergleich 0 sein.", "Anzahl", JOptionPane.ERROR_MESSAGE);
           	 return;
           }
            
        } catch (NumberFormatException n) {
            fwort.setText("0");
            JOptionPane.showMessageDialog(null, "Anzahl muss numerisch sein.", "Anzahl", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException();
        }
    }

    /**
     * beende den Dialog und generiere kein Raetsel.
     */
    private void abbrechen() {
        sollGenerieren = false;
        setVisible(false);
        dispose();
    }
    
    /**
     * gibt alle Woerter zurueck, ausser die die bereits ausgewaehlt wurden
     * 
     * @return restliche Wortliste
     */
    public Wortliste getRestWortliste(){
    	return wortliste;
    }
    
    /**
     * gibt die Wortliste zurueck, die die Woerter enthaelt die
     * auf jeden Fall im Raetsel vorkommen sollen
     * 
     * @return ausgewaehlte Woerter
     */
    public Wortliste getWortAusgewaehltListe(){
    	return wortAusgewaehlt;
    }
    
    /**
     * gibt die Anzahl der Fuellwoerter zurueck
     * 
     * @return Anzahl der Fuellwoerter
     */
    public short getAnzahlFuellWoerter(){
    	return fuellwoerter;
    }
    
    /**
     * gibt die boolesche Variable zurueck, ob das
     * Raetsel generiert werden kann.
     * 
     * @return true oder false
     */
    public boolean getErlaubnisZumGenerieren(){
    	return sollGenerieren;
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * Controller als innere private Klasse
     */
    private class GenListener implements ActionListener {
       
    	public void actionPerformed(ActionEvent action) {
            if (action.getSource() == ok) {
                try {
                    bestaetigen();
                }
                catch (NumberFormatException number) {}
                
            } else if (action.getSource() == beenden) {
                abbrechen();
            }
        }
    }
}
