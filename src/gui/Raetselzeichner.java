package gui;

import generator.Feld;
import generator.Feldstatus;
import generator.Raetselbauer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.LineMetrics;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.AttributedString;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import steuerung.Tupel;

/**
 * Diese Klasse ist fuer das Zeichnen des Raetsels zustaendig. 
 * Weiterhin besteht die Moeglichkeit das Raetsel zu drucken und es koennen 
 * Informationen fuer die Klasse SVGSchreiber gesammelt werden, damit das 
 * generierte Raetsel als SVG-Datei gespeichert werden kann.
 */
public class Raetselzeichner extends JPanel {
	
	// Bezugsobjekt
    private Raetselbauer kreuzwortdaten;
    
    /** Pixelgroesse einer Zelle */
    private final byte ZELLE = 64;
    private final Font BUCHSTABEN = new Font(Font.SANS_SERIF, Font.BOLD, 26);
    private final Font BESCHREIBUNG = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
    private boolean zeigeLoesung = false;
    
    /**
     * Der Konstruktor bekommt als Parameter das fertige Raetsel uebergeben
     * 
     * @param raetsel das fertige Kreuzwortraetsel
     */
    Raetselzeichner(Raetselbauer raetsel) {
        kreuzwortdaten = raetsel;
    }

    /**
     * Ruft die Methode auf, um das Raetsel zu zeichnen
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        zeichneRaetsel(g);
    }
    
    /**
	 * hier wird das Raetsel gezeichnet, indem mehrere Untermethoden aufgerufen
	 * werden, die je eine Teilaufgabe des Zeichnens uebernehmen
     * 
     * @param g das <code>Graphics</code>-Objekt
     */
    private void zeichneRaetsel(Graphics g) {
    	Graphics2D g2d = (Graphics2D) g;
        setBackground(Color.WHITE);
        zeichneGitternetz(g2d);
        zeichneGraueFelder(g2d); 
        zeichneSchrift(g2d);
        
        if (zeigeLoesung == true){
        	zeichneLoesung(g2d);
        }
    }
    
    /**
     * zeichnet das Gitternetz des Raetsels auf das JPanel, indem
     * vertikale und horizontale Lininen gezeichnet werden.
     * 
     * @param g Grafikobjekt
     */
    private void zeichneGitternetz(Graphics2D g) {
    	int xstart = 0;
    	int ystart = 0;
    	g.setColor(Color.BLACK);
        int xstop = xstart + ZELLE * kreuzwortdaten.getBreite();
        int ystop = ystart + ZELLE * kreuzwortdaten.getHoehe();
        
    	// zeichne horizontale Linien, beginnend von oben
        for (short i = 0; i <= kreuzwortdaten.getHoehe(); i++) {
            int y = ystart + i*ZELLE;
            g.drawLine(xstart, y, xstop-xstart, y);          
        }
        
    	// zeichne vertikale Linien, beginnend von links
        for (short i = 0; i <= kreuzwortdaten.getBreite(); i++) {
            int x = xstart + i*ZELLE;
            g.drawLine(x, ystart, x, ystop-ystart);
        }
    }
    
    /**
     * hier werden die grauen Felder emrittelt und grau eingefaerbt
     * 
     * @param g Graphikobjekt
     */
    private void zeichneGraueFelder(Graphics2D g) {
    	int xstart = 0;
    	int ystart = 0;
        // nicht benutzte Felder werden grau gezeichnet
        g.setColor(Color.GRAY);
        for (short x = 0; x < kreuzwortdaten.getBreite(); x++) {
            for (short y = 0; y < kreuzwortdaten.getHoehe(); y++) {
                if(kreuzwortdaten.getFeldBesetztMatrix()[y][x] == Feldstatus.UNGENUTZT | kreuzwortdaten.getFeldBesetztMatrix()[y][x] == Feldstatus.LEER) {
                    g.fillRect(xstart + x * ZELLE, ystart + y * ZELLE, ZELLE, ZELLE);                    
                }
            }
        }
    }
    
    /**
     * uebernimmt die Teilaufgabe die Felder schwarz zu faerben die eine Beschreibung
     * enthalten. Die Beschreibung wird hinzugefuegt.
     * 
     * @param g Graphikobjekt
     */
    private void zeichneSchrift(Graphics2D g){
    	int xstart = 0;
    	int ystart = 0;
        float breite = ZELLE - 5;
      
        /* Ein Datenfeldobjekt enthaelt alle benoetigten Informationen ueber ein Loesungswort
         * samt seiner Beschreibung und Wortverlaufsrichtung (horizontal, vertikal). Die
         * Beschreibungspositionen werden ermittelt wodurch das Raetsel, um die Beschreibungen
         * ergaenzt werden kann */
        for (Iterator<Feld> it = kreuzwortdaten.getRaetsel().iterator(); it.hasNext();) {      	
        	Feld feld = it.next();

        	/* Feldposition wo die Beschreibung steht */
            int x = feld.getBeschreibungKoordinate().getRechts();
            int y = feld.getBeschreibungKoordinate().getLinks();
            
            /* Faerbe das Feld schwarz fuer die Beschreibung */
            g.setColor(Color.BLACK);
            g.fillRect(xstart + x * ZELLE, ystart + y * ZELLE, ZELLE, ZELLE);

            /* Pfeil einzeichnen wo das Loesungswort startet */
            zeichnePfeil(g, feld, xstart, ystart);
            
            /* Beschreibung einzeichnen */
            FontRenderContext frc = g.getFontRenderContext();
            Font f = BESCHREIBUNG.deriveFont(BESCHREIBUNG.getSize2D());
            g.setFont(f);
            
            /* Die Farbe der Beschreibung */
            g.setColor(Color.WHITE);
            
            /* Die Loesungswortbeschreibung die gezeichnet werden soll */
            AttributedString attString = new AttributedString(feld.getBeschreibung());         
            attString.addAttribute(TextAttribute.FONT, f);
            LineBreakMeasurer lbm = new LineBreakMeasurer(attString.getIterator(), frc);
            Tupel<Integer> punkt = new Tupel<Integer>(xstart + x * ZELLE + 3, ystart + y * ZELLE);
                       
            while (lbm.getPosition() < feld.getBeschreibung().length()) {
                TextLayout layout = lbm.nextLayout(breite);

                punkt.rechts += (int) (layout.getAscent());
                float dx = 0;               
                if (layout.isLeftToRight() == true){
                	dx = 0;
                } else if (layout.isLeftToRight() == false){
                	dx = (breite - layout.getAdvance());
                }

                layout.draw(g, punkt.getLinks() + dx, punkt.getRechts());
                punkt.rechts += (int) layout.getDescent() + (int) layout.getLeading();
            }
        }
    }
    
    /**
     * hier werden die Pfeile eingezeichnet. Die Pfeile befinden sich in der
     * Startkoordinate des jeweiligen Loesungswortes.
     * 
     * @param g Graphikobjekt
     * @param feld Positionsbeschreibung des aktuellen Loesungswortes
     * @param xstart x-Koordinate, abhaengig von der jeweiligen Zelle
     * @param ystart y-Koordinate, abhaengig von der jeweiligen Zelle 
     */
    private void zeichnePfeil(Graphics2D g, Feld feld, int xstart, int ystart) {
        int x = xstart + feld.getStartKoordinate().getRechts() * ZELLE;
        int y = ystart + feld.getStartKoordinate().getLinks() * ZELLE;
        final int DIFF = 6;

        if(feld.getRichtung() == Feldstatus.HORIZONTAL) {
        	
        	/* Pfeil wird beginnend von oben nach rechts gezeichnet */
            if(feld.getBeschreibungKoordinate().getLinks()+1 == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts() == feld.getStartKoordinate().getRechts()) {
            	g.drawLine(x+DIFF, y, x+DIFF, y+ZELLE/2);
            	g.drawLine(x+DIFF, y+ZELLE/2, x+2*DIFF, y+ZELLE/2);
            	
            	Polygon p = getPolygon(x+2*DIFF,y+4*DIFF,x+2*DIFF,y+6*DIFF,x+3*DIFF,y+5*DIFF);
            	g.fillPolygon(p);
                
            /* Pfeil wird beginnend von unten nach rechts gezeichnet */   
            } if(feld.getBeschreibungKoordinate().getLinks()-1 == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts()== feld.getStartKoordinate().getRechts()) {
            	g.drawLine(x+DIFF, y+ZELLE/2, x+DIFF, y+ZELLE);
            	g.drawLine(x+DIFF, y+ZELLE/2, x+2*DIFF, y+ZELLE/2);

            	Polygon p = getPolygon(x+2*DIFF,y+4*DIFF,x+2*DIFF,y+6*DIFF,x+3*DIFF,y+5*DIFF);
            	g.fillPolygon(p);

            /* Pfeil wird nach unten gezeichnet */
            } if(feld.getBeschreibungKoordinate().getLinks() == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts() + 1 == feld.getStartKoordinate().getRechts()) {
            	Polygon p = getPolygon(x,y+4*DIFF,x,y+6*DIFF,x+DIFF,y+5*DIFF);
            	g.fillPolygon(p);
            }            
        } else if(feld.getRichtung() == Feldstatus.VERTIKAL){           
        	
        	/* Pfeil wird beginnend von links nach unten gezeichnet */
        	if(feld.getBeschreibungKoordinate().getLinks() == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts() + 1 == feld.getStartKoordinate().getRechts()) {
            	g.drawLine(x, y+DIFF, x+ZELLE/2, y+DIFF);
            	g.drawLine(x+ZELLE/2, y+DIFF, x+ZELLE/2, y+2*DIFF);
            	
            	Polygon p = getPolygon(x+4*DIFF,y+2*DIFF,x+6*DIFF,y+2*DIFF,x+5*DIFF,y+3*DIFF);
            	g.fillPolygon(p);

            /* Pfeil wird beginnend von rechts nach unten gezeichnet */
            } if(feld.getBeschreibungKoordinate().getLinks() == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts() - 1 == feld.getStartKoordinate().getRechts()) {
            	g.drawLine(x+ZELLE/2, y+DIFF, x+ZELLE, y+DIFF);
            	g.drawLine(x+ZELLE/2, y+DIFF, x+ZELLE/2, y+2*DIFF);
            	
            	Polygon p = getPolygon(x+4*DIFF,y+2*DIFF,x+6*DIFF,y+2*DIFF,x+5*DIFF,y+3*DIFF);
            	g.fillPolygon(p);

            /* Pfeil wird nach unten gezeichnet */
            } if(feld.getBeschreibungKoordinate().getLinks() +1 == feld.getStartKoordinate().getLinks() & feld.getBeschreibungKoordinate().getRechts() == feld.getStartKoordinate().getRechts()) {                    
            	Polygon p = getPolygon(x+4*DIFF,y,x+6*DIFF,y,x+5*DIFF,y+DIFF);
            	g.fillPolygon(p);
            }
        }
    }
    
    /**
     * Hilfsmethode, um Polygone zu erzeugen
     * @param px1 x-Koordinate des ersten Punkts
     * @param py1 y-Koordinate des ersten Punkts
     * @param px2 x-Koordinate des zweiten Punkts
     * @param py2 y-Koordinate des zweiten Punkts
     * @param px3 x-Koordinate des dritten Punkts
     * @param py3 y-Koordinate des dritten Punkts
     * @return Polygon
     */
    private Polygon getPolygon(int px1, int py1, int px2, int py2, int px3, int py3){
    	Polygon p = new Polygon();
    	p.addPoint(px1,py1);
       	p.addPoint(px2,py2);
       	p.addPoint(px3,py3);
       	return p;
    }
    
    /**
     * zeigt die Loesung des generierten Raetsels an. Die Loesung kann wider
     * verborgen werden.
     * 
     * @param g Graphikkontext
     */
    private void zeichneLoesung(Graphics2D g) {
    	int xstart = 0;
    	int ystart = 0;
        Font f = BUCHSTABEN.deriveFont(BUCHSTABEN.getSize2D());
        g.setFont(f);
        FontRenderContext frc = g.getFontRenderContext();
        g.setColor(Color.BLACK);
        for (short x = 0; x < kreuzwortdaten.getBreite(); x++) {
            for (short y = 0; y < kreuzwortdaten.getHoehe(); y++) {
            	
            	/* Ermittel fuer jedes Feldelement den Buchstaben */
                String s = Character.toString(kreuzwortdaten.getBuchstabenMatrix()[y][x]);
                Rectangle2D r2D = f.getStringBounds(s, frc);
                LineMetrics lm = f.getLineMetrics(s, frc);
                float breite = (float) r2D.getWidth();
                float hoehe = lm.getHeight();
                float as = lm.getAscent();
                float x0 = xstart + x * ZELLE +(ZELLE - breite)/2;
                float y0 = ystart + y * ZELLE +(ZELLE - hoehe)/2 + as;
                g.drawString(s, x0, y0);              
            }
        }
    }
    
    /**
     * ermittel die passende Groesse fuer das Raetsel
     */
    public Dimension getPreferredSize() {
        return new Dimension(ZELLE*kreuzwortdaten.getBreite(),ZELLE*kreuzwortdaten.getHoehe());
    }
    
    /**
     * sammelt Daten, um das speichern des Kreuzwortraetsels als
     * png-Datei zu ermoeglichen.
     */
    void speicherRaetsel(File datei) throws IOException {
        int bildbreite = getPreferredSize().width;
        int bildhoehe = getPreferredSize().height;
        BufferedImage bild = new BufferedImage(bildbreite, bildhoehe, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g = bild.createGraphics();
        g.setBackground(Color.WHITE);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, bildbreite, bildhoehe);
        zeichneRaetsel(g);
        if (zeigeLoesung == true) {
            zeichneLoesung(g);
        }
        ImageIO.write(bild, "png", datei);
    }
    /**
     * setzt den booleschen Wert, ob die Loesung gezeigt werden soll
     * oder nicht
     * @param loesungAnzeigen	true oder false
     */
    public void setZeigeLoesung(boolean loesungAnzeigen){
    	this.zeigeLoesung = loesungAnzeigen;
    }
    
    /**
     * ob die Loesung des Raetsel angezeigt werden soll
     * @return true, loesung anzeigen
     */
    public boolean getZeigeLoesung(){
    	return zeigeLoesung;
    }
}
