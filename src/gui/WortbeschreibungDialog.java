package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import steuerung.Tupel;

/**
 * Hier ist das Fenster fuer den Fall, dass aus mehreren Wortbeschreibungen zu einem gegebenen
 * Loesungsort eine auszuwaehlen ist. Dies wird nur dann der Fall sein, wenn ein Raetsel
 * per Wortauswahl generiert werden soll und es zu einem Loesungswort mehrere
 * Beschreibungen gibt
 */
public class WortbeschreibungDialog extends JDialog {
	
	/** Enthaelt Loesungswortbeschreibungen */
    private HashSet<String> beschreibungSet;	
    
    // Swing Komponenten
    private JButton ok,beenden;
    private JLabel wortLabel = new JLabel();
    
    /** zur Anzeige und Auswahl der einzelnen Woertern */
    private JList<String> beschreibungsliste = new JList<String>();
    
    /** verwaltet die Eintraege einer JList */
    private DefaultListModel<String> beschreibungListenmodell = new DefaultListModel<String>();
    
    /** Enthaelt alle Wortbeschreibungen zu ein Loesongswort */
    private HashSet<String> beschreibungsauswahlSet = new HashSet<String>();
    private boolean speichern = false;
    
    /**
     * Der Konstruktor baut ein Fenster auf mit den Loesungswort samt Beschreibungen, um eine
     * Beschreibung auszuwaehlen.
     * 
     * @param auswahldialog Um das Bedienen mehrerer Fenster zu unterbinden.
     * @param loesungswort Das Loesungswort
     * @param beschreibungen alle Beschreibungen fuer das uebergebene Loesungswort
     */
    public WortbeschreibungDialog(JDialog auswahldialog, String loesungswort, HashSet<String> beschreibungen) {
        super(auswahldialog, "Bitte Beschreibung waehlen", true);
        wortLabel.setText(loesungswort);      
        initKomponenten();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(getAufloesung().getLinks(),getAufloesung().getRechts());
        
        Auswahllistener listener = new Auswahllistener();
        listenerInit(listener, listener);
        
        /* Fuege den Listenmodell alle Beschreibungen des Loesungsortes hinzu */
        beschreibungSet = beschreibungen;
        for (Iterator<String> it = beschreibungSet.iterator(); it.hasNext();) {
            beschreibungListenmodell.addElement(it.next());
        }
    }
    
    /**
     * Fuegt allen Swing-Komponenten einen Listenerobjekt hinzu
     * @param action	Ein Actionlistener-Objekt
     * @param llistener	Ein ListSelectionListener-Objekt
     */
    private void listenerInit(ActionListener action, ListSelectionListener llistener){
        ok.addActionListener(action);
        beenden.addActionListener(action);
        beschreibungsliste.addListSelectionListener(llistener);
    }
    
    /** Initialsiert die JButtons */
    private void initKomponenten(){
        ok   = new JButton("OK");
        beenden = new JButton("Abbrechen");
    }
    
    /**
     * baut das Fenster auf
     */
    private void erzeugeFenster() {
    	JPanel panel = new JPanel();
    	panel.setLayout(new BorderLayout());
    	panel.add(wortLabel, BorderLayout.NORTH);
    	
        beschreibungListenmodell = new DefaultListModel<String>();
        beschreibungsliste = new JList<String>(beschreibungListenmodell);
        beschreibungsliste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        beschreibungsliste.setSelectedIndex(0);
        beschreibungsliste.setVisibleRowCount(5);
        panel.add(new JScrollPane(beschreibungsliste), BorderLayout.CENTER);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(ok);
        buttonPanel.add(beenden);
        panel.add(buttonPanel, BorderLayout.SOUTH);        
        add(panel);
        
        ok.setEnabled(false);
    }
    
    /**
     * beendet den Dialog samt Wortbeschreibungsauswahl fuer das
     * aktuelle Loesungswort
     */
    private void bestaetigen() {
        speichern = true;
        beschreibungsauswahlSet.add(beschreibungsliste.getSelectedValue());
        setVisible(false);
    }

    /**
     * bricht den Dialog ab. Es wurde keine Beschreibung ausgewaehlt.
     */
    private void abbrechen() {
        speichern = false;
        setVisible(false);
        dispose();
    }
    
    /**
     * 
     * @return alle Beschreibungen zu ein Loesungswort
     */
    public HashSet<String> getBeschreibungSet(){
    	return beschreibungsauswahlSet;
    }
    
    /**
     * @return true, wenn Beschreibung ausgewaehlt wurde, sonst false
     */
    public boolean getSpeichererlaubnis(){
    	return speichern;
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * Hier ist der Controller fuer diese Klasse
     */
    private class Auswahllistener implements ActionListener, ListSelectionListener {
        public void actionPerformed(ActionEvent action) {
            if (action.getSource() == ok) {
                bestaetigen();
            } else if (action.getSource() == beenden) {
                abbrechen();
            }
        }
        
        public void valueChanged(ListSelectionEvent e) {
            JList<String> liste = (JList<String>)e.getSource();
            if (liste.isSelectionEmpty() == true) {
            	
            	/* Es muss eine Auswahl getroffen werden */
                ok.setEnabled(false);
            } else {
                ok.setEnabled(true);           
            }
        }
    }

}
